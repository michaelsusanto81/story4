from django.urls import path, include
from . import views

app_name = "michaelapp"

urlpatterns = [
    path('', views.index, name='index'),
    path('portfolio/', views.portfolio, name='portfolio'),
    path('about/', views.about, name='about'),
    path('hireMe/', views.hireMe, name='hireMe'),
]