from django.shortcuts import render

# Create your views here.
def index(request):
	return render(request, 'index.html')

def portfolio(request):
	response = {'':''}
	return render(request, 'portfolio.html')

def about(request):
	response = {'':''}
	return render(request, 'about.html')

def hireMe(request):
	response = {'':''}
	return render(request, 'hire-me.html')